from flask.ext.wtf import Form
from wtforms import TextField, TextAreaField
from wtforms.validators import Required


class IdeaForm(Form):
    idea = TextAreaField('idea', [Required()])
    category = TextField('category', [Required()])

