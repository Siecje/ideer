from sqlalchemy.orm import relationship, backref
from app import db


association_table = db.Table('categories',
    db.Column('category_id', db.Integer, db.ForeignKey('ideer_category.id')),
    db.Column('idea_id', db.Integer, db.ForeignKey('ideer_idea.id'))
)

class Category(db.Model):
    __tablename__ = 'ideer_category'
    id = db.Column(db.Integer, primary_key=True)
    #parent_id = db.ForeignKey(id)
    parent_id = db.Column(db.Integer, db.ForeignKey('ideer_category.id'))
    sub_categories = relationship('Category', remote_side=[id])
    title = db.Column(db.String(50))
    
    def __init__(self, title, parent=None):
        self.parent_id = parent
        self.title = title

    def __unicode__(self):
        return self.title


class Idea(db.Model):
    __tablename__ = 'ideer_idea'
    id = db.Column(db.Integer, primary_key=True)
    categories = relationship('Category', backref='ideas', secondary=association_table)
    text = db.Column(db.String(255))
    
    #def __init__(self, text, category):
    #    self.text = text
    #    self.categories.add(category)

    def __unicode__(self):
        return self.text[:15]