from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for
from app import db
from app.ideer.models import Category, Idea
from app.ideer.forms import IdeaForm


mod = Blueprint('ideer', __name__, url_prefix='/ideer')


@mod.route('/')
def home():
    return "Home Page"


@mod.route('/create-idea', methods=['GET', 'POST'])
def create_idea():
    form = IdeaForm()
    if form.validate_on_submit():
        #get categories
        categories = form.category.data.split(',')
        idea_categories = []
        for category in categories:
            c = Category.query.filter_by(title=category).first()
            if c:
                idea_categories.append(c)
            else:
                #since category doesn't exist create it
                c = Category(title=category)
                db.session.add(c)
                #TODO: remove commit
                db.session.commit()
                idea_categories.append(c)
        new_idea = Idea(text=form.idea.data)
        new_idea.categories = idea_categories

        db.session.add(new_idea)
        db.session.commit()

        #redirect to the category of the new idea
        #TODO: multiple categories
        #TODO: go to idea?
        return redirect(url_for('ideer.home'))
    return render_template('ideer/create_idea.html', form=form)


#TODO: use slugs
@mod.route('/category/<id>')
def view_category(id):
    category = Category.query.get(id)
    ideas = category.ideas
    #TODO: button to copy idea to clipboard
    return render_template('ideer/category.html', category=category, ideas=ideas)

#TODO: search (startswith) for categories


@mod.route('/category')
def list_categories():
    # find categories without a parent
    categories = Category.query.all()
    #TODO: do this in one query
    #TODO: how to change dom element when it is selected
    #TODO: how to save it
    #Category.query.join(ContainerClass).filter(ContainerClass.id == 5).all()
    return render_template('ideer/list_categories.html', categories=categories)
